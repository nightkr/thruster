use ignore::{types::TypesBuilder, WalkBuilder};
use kube::api::DynamicObject;
use serde::de::DeserializeSeed;
use snafu::{ResultExt, Snafu};
use std::{
    fmt::Display,
    fs::File,
    io::Read,
    marker::PhantomData,
    path::{Path, PathBuf},
};

use crate::{utils::iter::TryIterator, LABEL_PACKAGE, LABEL_TAG};

#[derive(Snafu, Debug)]
pub enum Error {
    #[snafu(display("failed to read manifest at {}", path.display()))]
    ReadManifest {
        source: std::io::Error,
        path: PathBuf,
    },
    #[snafu(display("failed to parse manifest at {}", path))]
    InvalidManifest {
        source: serde_yaml::Error,
        path: ManifestPath,
    },
    #[snafu(display("failed to index path"))]
    Indexing { source: ignore::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Clone)]
pub struct ManifestPath {
    pub file_path: PathBuf,
}

impl Display for ManifestPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.file_path.display(), f)
    }
}

pub struct Manifest {
    pub path: ManifestPath,
    pub object: DynamicObject,
}

fn read_yaml_definitions(path: &Path) -> Result<Vec<Manifest>> {
    let mut content = Vec::new();
    File::open(&path)
        .context(ReadManifest { path: &path })?
        .read_to_end(&mut content)
        .context(ReadManifest { path: &path })?;
    serde_yaml::Deserializer::from_slice(&content)
        .map(|deser| {
            let object = PhantomData
                .deserialize(deser)
                .with_context(|| InvalidManifest {
                    path: ManifestPath {
                        file_path: path.to_owned(),
                    },
                })?;
            Ok(Manifest {
                object,
                path: ManifestPath {
                    file_path: path.to_owned(),
                },
            })
        })
        .collect()
}

#[tracing::instrument]
pub fn read_manifests(path: &Path, package: &str, tag: &str) -> Result<Vec<Manifest>> {
    WalkBuilder::new(path)
        .standard_filters(false)
        .hidden(false)
        .types({
            let mut types = TypesBuilder::new();
            types
                .add("yaml", "*.y{,a}ml")
                .expect("Failed to add type selector");
            types
                .select("yaml")
                .build()
                .expect("Failed to build file selector")
        })
        .build()
        .filter(|entry| {
            entry
                .as_ref()
                .ok()
                .and_then(ignore::DirEntry::file_type)
                .map_or(false, |t| t.is_file())
        })
        .map(|file| read_yaml_definitions(file.context(Indexing)?.path()))
        .try_flatten()
        .map(|manifest| {
            let mut manifest = manifest?;
            manifest.object.metadata.labels.extend([
                (LABEL_PACKAGE.into(), package.into()),
                (LABEL_TAG.into(), tag.into()),
            ]);
            Ok(manifest)
        })
        .collect()
}
