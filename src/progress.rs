use std::{cell::RefCell, future::Future, io, pin::Pin, task::Poll, time::Duration};

use futures::{
    future::{self, FusedFuture},
    stream, FutureExt, Stream, StreamExt,
};
use indicatif::ProgressBar;
use pin_project::{pinned_drop, PinProject};
use tokio::time::{interval, Interval};
use tracing_subscriber::fmt::MakeWriter;

thread_local! {
    pub static ACTIVE_PROGRESS_BAR: RefCell<Option<ProgressBar>> = RefCell::new(None);
}

fn with_progress_bar<T>(bar: ProgressBar, f: impl FnOnce() -> T) -> T {
    ACTIVE_PROGRESS_BAR.with(|active_bar| {
        let old_bar = active_bar.replace(Some(bar));
        let res = f();
        active_bar.replace(old_bar);
        res
    })
}

#[derive(PinProject)]
#[pin(PinnedDrop)]
pub struct ProgressBarFuture<F: Future> {
    progress: ProgressBar,
    tick: Interval,
    #[pin]
    inner: future::Fuse<F>,
}

impl<F: Future> Future for ProgressBarFuture<F> {
    type Output = F::Output;

    fn poll(self: Pin<&mut Self>, cx: &mut std::task::Context<'_>) -> Poll<Self::Output> {
        let this = self.project();
        if this.tick.poll_tick(cx).is_ready() {
            this.progress.tick();
        }
        let inner = this.inner;
        let res = with_progress_bar(this.progress.clone(), || inner.poll(cx));
        if res.is_ready() {
            this.progress.finish_using_style()
        }
        res
    }
}

#[pinned_drop]
impl<F: Future> PinnedDrop for ProgressBarFuture<F> {
    fn drop(self: Pin<&mut Self>) {
        if !self.inner.is_terminated() {
            self.progress.abandon()
        }
    }
}

pub trait ProgressFutureExt: Future {
    fn progress_with(self, progress: ProgressBar) -> ProgressBarFuture<Self>
    where
        Self: Sized,
    {
        ProgressBarFuture {
            progress,
            tick: interval(Duration::from_millis(100)),
            inner: self.fuse(),
        }
    }
}

impl<F: Future> ProgressFutureExt for F {}

#[derive(PinProject)]
#[pin(PinnedDrop)]
pub struct ProgressBarStream<S: Stream> {
    progress: ProgressBar,
    tick: Interval,
    #[pin]
    inner: stream::Fuse<S>,
}

impl<S: Stream> Stream for ProgressBarStream<S> {
    type Item = S::Item;

    fn poll_next(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        let this = self.project();
        if this.tick.poll_tick(cx).is_ready() {
            this.progress.tick();
        }
        let inner = this.inner;
        let res = with_progress_bar(this.progress.clone(), || inner.poll_next(cx));
        match &res {
            Poll::Ready(Some(_)) => this.progress.inc(1),
            Poll::Ready(None) => this.progress.finish_using_style(),
            _ => {}
        }
        res
    }
}

#[pinned_drop]
impl<S: Stream> PinnedDrop for ProgressBarStream<S> {
    fn drop(self: Pin<&mut Self>) {
        if !self.inner.is_done() {
            self.progress.abandon()
        }
    }
}

pub trait ProgressStreamExt: Stream {
    fn progress_with(self, progress: ProgressBar) -> ProgressBarStream<Self>
    where
        Self: Sized,
    {
        ProgressBarStream {
            progress,
            tick: interval(Duration::from_millis(100)),
            inner: self.fuse(),
        }
    }
}

impl<S: Stream> ProgressStreamExt for S {}

pub struct ProgressWriter {
    progress: Option<ProgressBar>,
}

impl io::Write for ProgressWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if let Some(progress) = &self.progress {
            progress.println(
                std::str::from_utf8(buf)
                    .map_err(|err| std::io::Error::new(io::ErrorKind::InvalidData, err))?,
            );
            Ok(buf.len())
        } else {
            std::io::stderr().write(buf)
        }
    }

    fn flush(&mut self) -> io::Result<()> {
        if self.progress.is_none() {
            std::io::stderr().flush()?;
        }
        Ok(())
    }
}

pub struct ProgressMakeWriter;

impl MakeWriter for ProgressMakeWriter {
    type Writer = ProgressWriter;

    fn make_writer(&self) -> Self::Writer {
        ACTIVE_PROGRESS_BAR.with(|progress| ProgressWriter {
            progress: progress.borrow().clone(),
        })
    }
}
