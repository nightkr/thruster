pub mod iter {
    pub struct TryFlatten<I1, I2: IntoIterator> {
        outer: I1,
        inner: Option<I2::IntoIter>,
    }

    impl<I1, I2, E> Iterator for TryFlatten<I1, I2>
    where
        I1: Iterator<Item = Result<I2, E>>,
        I2: IntoIterator,
    {
        type Item = Result<I2::Item, E>;

        fn next(&mut self) -> Option<Self::Item> {
            loop {
                if let Some(inner) = &mut self.inner {
                    if let Some(value) = inner.next() {
                        break Some(Ok(value));
                    }
                }
                match self.outer.next()? {
                    Ok(inner) => self.inner = Some(inner.into_iter()),
                    Err(err) => break Some(Err(err)),
                }
            }
        }
    }

    pub trait TryIterator: Iterator {
        type Ok;
        type Error;

        fn try_flatten(self) -> TryFlatten<Self, Self::Ok>
        where
            Self: Sized,
            Self::Ok: IntoIterator,
        {
            TryFlatten {
                outer: self,
                inner: None,
            }
        }
    }

    impl<I, Ok, Err> TryIterator for I
    where
        I: Iterator<Item = Result<Ok, Err>>,
    {
        type Ok = Ok;
        type Error = Err;
    }
}
