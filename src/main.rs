#![deny(clippy::pedantic, clippy::unwrap_used)]
#![allow(clippy::module_name_repetitions)]

mod apply;
mod object_ref;
mod progress;
mod prune;
mod reader;
mod utils;

use crate::{
    apply::apply_manifests,
    object_ref::ObjectRef,
    progress::{ProgressFutureExt, ProgressMakeWriter, ProgressStreamExt},
    prune::{get_objects, prune_objects},
    reader::read_manifests,
};
use futures::{stream, TryStreamExt};
use indicatif::{ProgressBar, ProgressFinish, ProgressStyle};
use kube::{api::DynamicObject, discovery::ApiGroup, Discovery};
use snafu::{ResultExt, Snafu};
use std::{collections::HashMap, convert::TryFrom, path::PathBuf};
use structopt::StructOpt;
use tracing::{error, info_span};
use tracing_futures::Instrument;
use tracing_subscriber::{fmt::format::FmtSpan, EnvFilter};

pub const LABEL_PACKAGE: &str = "thruster.nullable.se/package";
pub const LABEL_TAG: &str = "thruster.nullable.se/tag";

#[derive(StructOpt)]
struct Opts {
    /// Path to a folder of manifests to apply
    manifests: PathBuf,
    /// Package ID, must be stable over the lifetime of the package
    #[structopt(long = "package")]
    package: String,
    /// Package version ID, should be a Git commit hash or similar
    #[structopt(long = "tag")]
    tag: String,
    /// Default Kubernetes namespace
    #[structopt(long = "namespace", short = "n")]
    namespace: Option<String>,
    /// Delete objects that are removed from the package
    #[structopt(long = "allow-delete")]
    allow_delete: bool,
}

#[derive(Snafu, Debug)]
enum Error {
    #[snafu(display("failed to init kube client"))]
    KubeInit { source: kube::Error },
    #[snafu(display("failed to discover resources from cluster"))]
    ResourceDiscoveryFailed { source: kube::Error },
    #[snafu(display("failed to scan for outdated objects"))]
    Scan { source: prune::Error },
    #[snafu(display("failed to read manifests"), context(false))]
    ReadManifests { source: reader::Error },
    #[snafu(display("failed to apply manifests"), context(false))]
    ApplyManifests { source: apply::Error },
    #[snafu(display("failed to prune objects"))]
    PruneObjects { source: prune::Error },
}
type Result<T, E = Error> = std::result::Result<T, E>;

#[tokio::main]
#[allow(clippy::unwrap_used)]
async fn run() -> Result<()> {
    tracing_subscriber::fmt()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .pretty()
        .with_writer(ProgressMakeWriter)
        .with_env_filter(EnvFilter::from_default_env())
        .init();
    let opts = Opts::from_args();
    let kube_config = kube::Config::infer().await.context(KubeInit)?;
    let kube = kube::Client::try_from(kube_config.clone()).context(KubeInit)?;

    let spinner_style = ProgressStyle::default_spinner()
        .template("{spinner} {msg}")
        .on_finish(ProgressFinish::AndLeave);
    let bar_style = ProgressStyle::default_bar()
        .template("{spinner} {msg} [{pos}/{len}] {wide_bar}")
        .on_finish(ProgressFinish::AndLeave);

    let discovery = Discovery::new(kube.clone())
        .run()
        .instrument(info_span!("discover_resources"))
        .progress_with(
            ProgressBar::new_spinner()
                .with_message("Discovering resources")
                .with_style(spinner_style.clone()),
        )
        .await
        .context(ResourceDiscoveryFailed)?;
    let resources = discovery
        .groups()
        .flat_map(ApiGroup::recommended_resources)
        .collect::<Vec<_>>();
    let mut known_objects = get_objects(&opts.package, None, &resources, &kube)
        .progress_with(
            ProgressBar::new(resources.len() as u64)
                .with_message("Finding outdated objects")
                .with_style(bar_style.clone()),
        )
        .map_ok(|objects| stream::iter(objects.into_iter().map(Ok)))
        .try_flatten()
        .try_collect::<HashMap<ObjectRef, DynamicObject>>()
        .await
        .context(Scan)?;
    let manifests = read_manifests(&opts.manifests, &opts.package, &opts.tag)?;
    let manifest_count = manifests.len();
    let updated_objects = apply_manifests(
        manifests,
        &opts.package,
        opts.namespace.as_deref(),
        &discovery,
        &kube,
        &kube_config,
    )
    .progress_with(
        ProgressBar::new(manifest_count as u64)
            .with_message("Applying manifests")
            .with_style(bar_style.clone()),
    )
    .try_collect::<Vec<_>>()
    .await?;
    known_objects.extend(updated_objects);
    prune_objects(
        &opts.package,
        &opts.tag,
        &resources,
        &kube,
        opts.allow_delete,
        &known_objects,
    )
    .progress_with(
        ProgressBar::new(resources.len() as u64)
            .with_message("Pruning objects")
            .with_style(bar_style),
    )
    .try_for_each(|_| async { Ok(()) })
    .await
    .context(PruneObjects)?;

    Ok(())
}

fn main() {
    match run() {
        Ok(()) => {}
        Err(err) => {
            error!(
                error = &err as &dyn std::error::Error,
                "Failed to apply thrusters"
            );
            std::process::exit(1);
        }
    }
}
