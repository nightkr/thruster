use std::collections::{hash_map::Entry, HashMap};

use futures::{stream, Stream, StreamExt};
use kube::{
    api::{DynamicObject, GroupVersionKind, Patch, PatchParams},
    discovery::Scope,
    Api, Discovery,
};
use snafu::{OptionExt, ResultExt, Snafu};
use tracing::{info, info_span};
use tracing_futures::Instrument;

use crate::{
    object_ref::{self, gvk_of_typemeta, ObjectRef},
    reader::{Manifest, ManifestPath},
};

#[derive(Snafu, Debug)]
pub enum Error {
    #[snafu(display("manifest at {} has no type", path))]
    TypelessManifest { path: ManifestPath },
    #[snafu(display("manifest at {} has no name", path))]
    NamelessManifest { path: ManifestPath },
    #[snafu(display("manifest at {} has no valid object ref", path))]
    NoObjectRef {
        source: object_ref::FromDynamicObjError,
        path: ManifestPath,
    },
    #[snafu(display("could not find resource {}.{}.{} in cluster", gvk.kind, gvk.version, gvk.group))]
    UnknownGroupVersionKind { gvk: GroupVersionKind },
    #[snafu(display("object {} is defined at both {} and {}", object_ref, first, second))]
    DuplicateManifest {
        object_ref: ObjectRef,
        first: ManifestPath,
        second: ManifestPath,
    },
    #[snafu(display("failed to apply object {} (at {})", object_ref, path))]
    Apply {
        source: kube::Error,
        object_ref: Box<ObjectRef>,
        path: ManifestPath,
    },
}
type Result<T, E = Error> = std::result::Result<T, E>;

fn check_manifest_duplication(manifests: &[Manifest]) -> Result<()> {
    let mut refs = HashMap::<ObjectRef, &Manifest>::new();
    for manifest in manifests {
        let obj_ref = ObjectRef::from_obj(&manifest.object).with_context(|| NoObjectRef {
            path: manifest.path.clone(),
        })?;
        match refs.entry(obj_ref) {
            Entry::Occupied(entry) => {
                return DuplicateManifest {
                    object_ref: entry.key().clone(),
                    first: entry.get().path.clone(),
                    second: manifest.path.clone(),
                }
                .fail()
            }
            Entry::Vacant(entry) => {
                entry.insert(manifest);
            }
        }
    }
    Ok(())
}

fn resolve_manifest_defaults(
    manifest: &mut Manifest,
    namespace: Option<&str>,
    kube_config: &kube::Config,
) {
    manifest.object.metadata.namespace.get_or_insert_with(|| {
        namespace.map_or_else(|| kube_config.default_namespace.clone(), str::to_string)
    });
}

pub fn apply_manifests<'a>(
    mut manifests: Vec<Manifest>,
    package: &'a str,
    namespace: Option<&'a str>,
    discovery: &'a Discovery,
    kube: &'a kube::Client,
    kube_config: &'a kube::Config,
) -> impl Stream<Item = Result<(ObjectRef, DynamicObject)>> + 'a {
    let span = info_span!(
        "apply_manifests",
        package,
        namespace = tracing::field::Empty
    );
    if let Some(namespace) = namespace {
        span.record("namespace", &namespace);
    }
    for manifest in &mut manifests {
        resolve_manifest_defaults(manifest, namespace, kube_config);
    }
    apply_manifest_group(manifests, package, discovery, kube).instrument(span)
}

fn apply_manifest_group<'a>(
    manifests: Vec<Manifest>,
    package: &'a str,
    discovery: &'a Discovery,
    kube: &'a kube::Client,
) -> impl Stream<Item = Result<(ObjectRef, DynamicObject)>> + 'a {
    match check_manifest_duplication(&manifests) {
        Err(err) => stream::once(async { Err(err) }).left_stream(),
        Ok(()) => {
            stream::iter(manifests)
                .map(move |mut manifest| async move {
                    // tokio::time::sleep(Duration::from_secs(1)).await;
                    let manifest_type =
                        manifest
                            .object
                            .types
                            .as_ref()
                            .with_context(|| TypelessManifest {
                                path: manifest.path.clone(),
                            })?;
                    let gvk = gvk_of_typemeta(manifest_type);
                    let (resource, capabilities) = discovery
                        .resolve_gvk(&gvk)
                        .context(UnknownGroupVersionKind { gvk })?;
                    let object_ref =
                        ObjectRef::from_obj(&manifest.object).with_context(|| NoObjectRef {
                            path: manifest.path.clone(),
                        })?;
                    info!(%object_ref, "Applying object");
                    let api: Api<DynamicObject> = match capabilities.scope {
                        Scope::Cluster => {
                            manifest.object.metadata.namespace = None;
                            Api::all_with(kube.clone(), &resource)
                        }
                        Scope::Namespaced => Api::namespaced_with(
                            kube.clone(),
                            manifest
                                .object
                                .metadata
                                .namespace
                                .as_deref()
                                .expect("manifest should have namespace"),
                            &resource,
                        ),
                    };
                    let name = manifest.object.metadata.name.as_deref().with_context(|| {
                        NamelessManifest {
                            path: manifest.path.clone(),
                        }
                    })?;
                    let object = api
                        .patch(
                            name,
                            &PatchParams::apply(&format!("thruster-{}", package)),
                            &Patch::Apply(&manifest.object),
                        )
                        .await
                        .with_context(|| Apply {
                            path: manifest.path.clone(),
                            object_ref: object_ref.clone(),
                        })?;
                    Ok((object_ref, object))
                })
                .buffer_unordered(5)
                .right_stream()
        }
    }
}
