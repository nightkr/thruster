use std::collections::HashMap;

use crate::{
    object_ref::{self, ObjectRef},
    LABEL_PACKAGE, LABEL_TAG,
};
use futures::{stream, Stream, StreamExt};
use kube::{
    api::{ApiResource, DeleteParams, DynamicObject, ListParams, TypeMeta},
    discovery::ApiCapabilities,
    Api,
};
use snafu::{ResultExt, Snafu};
use tracing::{info, info_span};
use tracing_futures::Instrument;

#[derive(Snafu, Debug)]
pub enum Error {
    #[snafu(display("failed to list objects of resource {}.{}.{}", resource.kind, resource.version, resource.group))]
    List {
        source: kube::Error,
        resource: ApiResource,
    },
    #[snafu(display("failed to delete object {}", object_ref))]
    Delete {
        source: kube::Error,
        object_ref: ObjectRef,
    },
    #[snafu(display(
        "would prune removed object {}, run again with --allow-delete to allow",
        object_ref
    ))]
    DeleteDisallowed { object_ref: ObjectRef },
    #[snafu(display("refusing to prune object {} that has been retagged since Thruster started (changed from {:?} to {:?})", object_ref, old_tag, new_tag))]
    Conflict {
        object_ref: ObjectRef,
        old_tag: Option<String>,
        new_tag: Option<String>,
    },
    #[snafu(display("object has no valid object ref: {}", source))]
    NoObjectRef {
        source: object_ref::FromDynamicObjError,
    },
}
type Result<T, E = Error> = std::result::Result<T, E>;

async fn get_objects_of_resource(
    resource: &ApiResource,
    capabilities: &ApiCapabilities,
    package: &str,
    skip_tag: Option<&str>,
    kube: &kube::Client,
) -> Result<Vec<(ObjectRef, DynamicObject)>> {
    if !capabilities.supports_operation("list") {
        info!("Skipping resource because listing is not supported");
        return Ok(Vec::new());
    }
    let api = Api::<DynamicObject>::all_with(kube.clone(), &resource);
    let skip_tag_selector = if let Some(skip_tag) = skip_tag {
        format!(",{}!={}", LABEL_TAG, skip_tag)
    } else {
        format!("")
    };
    api.list(&ListParams {
        label_selector: Some(format!(
            "{}={}{}",
            LABEL_PACKAGE, package, skip_tag_selector
        )),
        ..ListParams::default()
    })
    .await
    .with_context(|| List {
        resource: resource.clone(),
    })?
    .items
    .into_iter()
    .map(|mut obj| {
        obj.types = Some(TypeMeta {
            api_version: resource.api_version.clone(),
            kind: resource.kind.clone(),
        });
        Ok((ObjectRef::from_obj(&obj).context(NoObjectRef)?, obj))
    })
    .collect()
}

pub fn get_objects<'a>(
    package: &'a str,
    skip_tag: Option<&'a str>,
    resources: &'a [(ApiResource, ApiCapabilities)],
    kube: &'a kube::Client,
) -> impl Stream<Item = Result<Vec<(ObjectRef, DynamicObject)>>> + 'a {
    let span = info_span!("get_objects", package, skip_tag = tracing::field::Empty);
    if let Some(skip_tag) = skip_tag {
        span.record("skip_tag", &skip_tag);
    }
    stream::iter(resources.iter())
        .map(move |(resource, capabilities)| {
            get_objects_of_resource(resource, capabilities, package, skip_tag, &kube)
        })
        .buffer_unordered(5)
        .instrument(span)
}

#[tracing::instrument(
    skip(resource, capabilities, kube, known_objects),
    fields(resource = %format_args!("{}.{}.{}", resource.kind.as_str(), resource.version.as_str(), resource.group.as_str()))
)]
async fn prune_objects_of_resource(
    resource: &ApiResource,
    capabilities: &ApiCapabilities,
    package: &str,
    tag: &str,
    kube: &kube::Client,
    allow_delete: bool,
    known_objects: &HashMap<ObjectRef, DynamicObject>,
) -> Result<()> {
    let objects_to_delete =
        get_objects_of_resource(resource, capabilities, package, Some(tag), kube).await?;
    for (object_ref, obj) in objects_to_delete {
        let namespace = object_ref.namespace.as_ref();
        let api = match namespace {
            Some(ns) => Api::namespaced_with(kube.clone(), ns, resource),
            None => Api::<DynamicObject>::all_with(kube.clone(), &resource),
        };
        if allow_delete {
            let current_object_tag = obj.metadata.labels.get(LABEL_TAG);
            let old_object_tag = known_objects
                .get(&object_ref)
                .and_then(|obj| obj.metadata.labels.get(LABEL_TAG));
            if current_object_tag != old_object_tag {
                return Conflict {
                    object_ref,
                    old_tag: old_object_tag.cloned(),
                    new_tag: current_object_tag.cloned(),
                }
                .fail();
            }

            info!(%object_ref, "Pruning deleted object");
            api.delete(&object_ref.name, &DeleteParams::default())
                .await
                .with_context(move || Delete { object_ref })?;
        } else {
            return DeleteDisallowed { object_ref }.fail();
        }
    }
    Ok(())
}

pub fn prune_objects<'a>(
    package: &'a str,
    tag: &'a str,
    resources: &'a [(ApiResource, ApiCapabilities)],
    kube: &'a kube::Client,
    allow_delete: bool,
    known_objects: &'a HashMap<ObjectRef, DynamicObject>,
) -> impl Stream<Item = Result<()>> + 'a {
    stream::iter(resources.iter())
        .map(move |(resource, capabilities)| async move {
            prune_objects_of_resource(
                resource,
                capabilities,
                package,
                tag,
                &kube,
                allow_delete,
                known_objects,
            )
            .await
        })
        .buffer_unordered(5)
        .instrument(info_span!("prune_objects", package, tag))
}
