use kube::api::{DynamicObject, GroupVersionKind, TypeMeta};
use snafu::{OptionExt, Snafu};
use std::fmt::Display;

#[derive(Snafu, Debug)]
pub enum FromDynamicObjError {
    #[snafu(display("object has no type"))]
    UntypedObject,
    #[snafu(display("object has no name"))]
    UnnamedObject,
}

/// Unversioned reference to a dynamic object
#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct ObjectRef {
    pub group: String,
    pub kind: String,
    pub namespace: Option<String>,
    pub name: String,
}

impl ObjectRef {
    pub fn from_obj(obj: &DynamicObject) -> Result<Self, FromDynamicObjError> {
        let tpe = obj.types.as_ref().context(UntypedObject)?;
        let gvk = gvk_of_typemeta(tpe);
        Ok(Self {
            group: gvk.group,
            kind: gvk.kind,
            namespace: obj.metadata.namespace.clone(),
            name: obj.metadata.name.clone().context(UnnamedObject)?,
        })
    }
}

impl Display for ObjectRef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}.{}/{}", self.kind, self.group, self.name))?;
        if let Some(ns) = self.namespace.as_ref() {
            f.write_fmt(format_args!(".{}", ns))?;
        }
        Ok(())
    }
}

pub fn gvk_of_typemeta(tpe: &TypeMeta) -> GroupVersionKind {
    match tpe.api_version.split_once('/') {
        Some((group, version)) => GroupVersionKind::gvk(&group, &version, &tpe.kind),
        None => GroupVersionKind::gvk("", &tpe.api_version, &tpe.kind),
    }
}
